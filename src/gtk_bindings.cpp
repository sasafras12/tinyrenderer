//g++ gtk_bindings.cpp noiseutils.cpp -I../include/ -g `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` -O2 -lpng -lnoise -o gtk_bindings 

#include <gtk/gtk.h>
#include "maths.h"
#include <math.h>
#include <algorithm>
#include <list>
#include <fstream>
#include "raytracer3.cpp"

GtkWidget *window, *box;
GtkWidget *vbox;
GtkWidget *menuBar;
GtkWidget *fileMenu;
GtkWidget *fileMi;
GtkWidget *quitMi;
const int width = 640, height = 480;
const char *file;
const char *obj_file;
GdkPixbuf *pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, width, height);
int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
guchar *pixels = gdk_pixbuf_get_pixels (pixbuf);
GtkImage *image;

mesh meshCube;
mat4x4 matProj;
mat4x4 matRotZ, matRotX;
mat4x4 matTrans;
mat4x4 matWorld;
mat4x4 matCameraRotY;
mat4x4 matCameraRotX;
mat4x4 matCameraRot;
mat4x4 matCamera;
mat4x4 matView;

double fTheta = 0;
double fYaw;
double fPitch;
float z = 2.0f;
vec3d vCamera;
vec3d vLookDir;
vec3d vForward;
vec3d vLeft = {1, 0, 0};
vec3d vRight = {-1, 0, 0};
vec3d light_direction = {-7, 10, -10 };
vec3d vUp = { 0,1,0 };
vec3d vTarget = { 0,0,0 };


Sprite s;
float *pDepthBuffer = nullptr;
int SPRITE_SIZE_W;
int SPRITE_SIZE_H;

static gboolean is_w_pressed = FALSE;
static gboolean is_a_pressed = FALSE;
static gboolean is_s_pressed = FALSE;
static gboolean is_d_pressed = FALSE;
static gboolean is_q_pressed = FALSE;
static gboolean is_e_pressed = FALSE;


long int old_time = 0;

float last_position = 0;
std::vector<Triangle*> tris_to_delete;
std::vector<triangle> allTriangles;

static gboolean mouse_scroll(GtkWidget *widget, GdkEventScroll *event, gpointer user_data)
{
	std::cout << "Scrolled!" << std::endl;
	return TRUE;
}

Colour getColourAt(vec intersection_position, vec intersecting_ray_direction,std::vector<Object*> scene_objects, int index_of_winning_object, std::vector<Source*> light_sources, double accuracy, double ambientlight) {
	Colour winning_object_colour = scene_objects.at(index_of_winning_object)->getColour();
	vec winning_object_normal = scene_objects.at(index_of_winning_object)->getNormalAt(intersection_position);


	if (winning_object_colour.getColourSpecial() == 2) {
		// change colour of pixel
		winning_object_colour.setColourRed((float)(s.getColour(intersection_position.getVecX()*1023, intersection_position.getVecY()*1023)[0])/255);
		winning_object_colour.setColourGreen((float)(s.getColour(intersection_position.getVecX()*1023, intersection_position.getVecY()*1023)[1])/255);
		winning_object_colour.setColourBlue((float)(s.getColour(intersection_position.getVecX()*1023, intersection_position.getVecY()*1023)[2])/255);

	}

	Colour final_colour = winning_object_colour.colourScalar(ambientlight);

	if (winning_object_colour.getColourSpecial() > 0 && winning_object_colour.getColourSpecial() <= 1) {
		// reflection from objects with specular intensity
		double dot1 = winning_object_normal.dotProduct(intersecting_ray_direction.negative());
		vec scalar1 = winning_object_normal.vecMul(dot1);
		vec add1 = scalar1.vecAdd(intersecting_ray_direction);
		vec scalar2 = add1.vecMul(2);
		vec add2 = intersecting_ray_direction.negative().vecAdd(scalar2);
		vec reflection_direction = add2.normalize();
		
		Ray reflection_ray (intersection_position, reflection_direction);
		// determine what the ray intersects with first
		std::vector<double> reflection_intersections;
		for (int reflection_index = 0; reflection_index < scene_objects.size(); reflection_index++) {
			reflection_intersections.push_back(scene_objects.at(reflection_index)->findIntersection(reflection_ray));		
		}

		int index_of_winning_object_with_reflection = winningObjectIndex(reflection_intersections);
		if (index_of_winning_object_with_reflection != -1) {
			// reflection ray missed everything else
			if (reflection_intersections.at(index_of_winning_object_with_reflection) > accuracy) {
				// determine the position and direction at the point of intersection with the reflection ray
				// the ray only affects the colour if it reflected off something
				vec reflection_intersection_position = intersection_position.vecAdd(reflection_direction.vecMul(reflection_intersections.at(index_of_winning_object_with_reflection)));
				vec reflection_intersection_ray_direction = reflection_direction;
				Colour reflection_intersection_colour = getColourAt(reflection_intersection_position, reflection_intersection_ray_direction, scene_objects, index_of_winning_object_with_reflection, light_sources, accuracy, ambientlight );

				final_colour = final_colour.colourAdd(reflection_intersection_colour.colourScalar(winning_object_colour.getColourSpecial()));
			}
		}	
	}

	for (int light_index = 0; light_index < light_sources.size(); light_index++) {
		vec light_direction = light_sources.at(light_index)->getLightPosition().vecAdd(intersection_position.negative()).normalize();
		float cosine_angle = winning_object_normal.dotProduct(light_direction);

		if (cosine_angle > 0) {
			// test for shadows
			bool shadowed = false;
			vec distance_to_light = light_sources.at(light_index)->getLightPosition().vecAdd(intersection_position.negative()).normalize();
			float distance_to_light_magnitude = distance_to_light.magnitude();
			
			Ray shadow_ray (intersection_position, light_sources.at(light_index)->getLightPosition().vecAdd(intersection_position.negative()).normalize());

			std::vector<double> secondary_intersections;

			for (int object_index = 0; object_index < scene_objects.size() && shadowed == false; object_index++)
			{
				secondary_intersections.push_back(scene_objects.at(object_index)->findIntersection(shadow_ray));
			}

			for (int c = 0; c < secondary_intersections.size(); c++) {
				if (secondary_intersections.at(c) > accuracy) {
					if (secondary_intersections.at(c) <= distance_to_light_magnitude) {
						shadowed = true;
					}
					break;
				}

			}
			if (shadowed == false) {
				final_colour = final_colour.colourAdd(winning_object_colour.colourMultiply(light_sources.at(light_index)->getLightColour().colourScalar(cosine_angle)));

				if (winning_object_colour.getColourSpecial() > 0 && winning_object_colour.getColourSpecial() <= 1) {
					// special [0-1]
					double dot1 = winning_object_normal.dotProduct(intersecting_ray_direction.negative());
					vec scalar1 = winning_object_normal.vecMul(dot1);
					vec add1 = scalar1.vecAdd(intersecting_ray_direction);
					vec scalar2 = add1.vecMul(2);
					vec add2 = intersecting_ray_direction.negative().vecAdd(scalar2);
					vec reflection_direction = add2.normalize();
					double specular = reflection_direction.dotProduct(light_direction);
					if (specular > 0) {
						specular = pow(specular, 10);
						final_colour = final_colour.colourAdd(light_sources.at(light_index)->getLightColour().colourScalar(specular*winning_object_colour.getColourSpecial()));
					}
				}
			}
		}
	}

	return final_colour.clip();
}

void render(float cx, float cy, float cz, float lx, float ly, float lz) {

	std::cout << "rendering..." << std::endl;

	clock_t time1, time2;
	time1 = clock();
	
	int width = 640;
	int height = 480;
	int dpi = 72;
	double aspectratio = (double)width/(double)height;
	int n = width * height;
	RGBType pixels[n];
	int aadepth = 1;
	double aathreshold = 0.1;
	double ambientlight = 0.2;
	double accuracy = 0.000001;



	vec o(0, 0, 0);
	vec x(1, 0, 0);
	vec y(0, 1, 0);
	vec z(0, 0, 1);
	//for (int i = 0; i < 30; i++) {



		vec campos(-cx, cy, cz);

		vec look_at (lx, ly, lz);
		vec diff_btw(campos.getVecX() - look_at.getVecX(), campos.getVecY() - look_at.getVecY(),campos.getVecZ() - look_at.getVecZ());

		vec camdir = diff_btw.negative().normalize();
		vec camright = y.crossProduct(camdir).normalize();
		vec camdown = camright.crossProduct(camdir);
		Camera scene_cam (campos, camdir, camright, camdown);
		Colour white_light (1.0, 1.0, 1.0, 0);
		Colour pretty_green (0.5, 1.0, 0.5, 0.3);
		Colour maroon(0.5, 0.25, 0.25, 0);
		Colour tile_floor(0.5, 0.25, 0.25, 0);
		Colour gray (0.5, 0.5, 0.5, 0);
		Colour black(0.0, 0.0, 0.0, 0);
		
		vec light_position(-7, 10, -10);
		Light scene_light (light_position, white_light);
		std::vector<Source*> light_sources;
		light_sources.push_back(dynamic_cast<Source*>(&scene_light));
		std::vector<Object*> scene_objects;
		// scene objects
		Sphere scene_sphere (o, 1, pretty_green);
		Plane scene_plane (y, -1, tile_floor);
		Colour colour(1, 1, 1, 2);
		Triangle scene_triangle(vec(1, 0, 0), vec(0, 1, 0), vec(0, 0, 1), colour);

		//scene_objects.push_back(dynamic_cast<Object*>(&scene_sphere));
		scene_objects.push_back(dynamic_cast<Object*>(&scene_plane));
		scene_objects.push_back(dynamic_cast<Object*>(&scene_triangle));
		//makeCube(vec(1, 1, 1), vec(-1, -1, -1), maroon);

		/*vec corner1 = vec(1, 1, 1);
		vec corner2 = vec(-1, -1, -1);

		// corner1
		double c1x = corner1.getVecX();
		double c1y = corner1.getVecY();
		double c1z = corner1.getVecZ();
		// corner2
		double c2x = corner2.getVecX();
		double c2y = corner2.getVecY();
		double c2z = corner2.getVecZ();

		vec A (c2x, c1y, c1z);
		vec B (c2x, c1y, c2z);
		vec C (c1x, c1y, c2z);

		vec D (c2x, c2y, c1z);
		vec E (c1x, c2y, c1z);
		vec F (c1x, c2y, c2z);


		Triangle t1(D, A, corner1, colour) ;
		Triangle t2(corner1, E, D, colour);
		Triangle t3(corner2, B, A, colour);
		Triangle t4(A, D, corner2, colour);
		Triangle t5(F, C, B, colour);
		Triangle t6(B, corner2, F, colour);
		Triangle t7(E, corner1, C, colour);
		Triangle t8(C, F, E, colour);
		Triangle t9(D, E, F, colour);
		Triangle t10(F, corner2, D, colour);
		Triangle t11(corner1, A, B, colour);
		Triangle t12(B, C, corner1, colour);


		// left side
		scene_objects.push_back( dynamic_cast<Object*>(&t1) );
		scene_objects.push_back( dynamic_cast<Object*>(&t2) );
		// far side
		scene_objects.push_back( dynamic_cast<Object*>(&t3) );
		scene_objects.push_back( dynamic_cast<Object*>(&t4) );
		// right side
		scene_objects.push_back( dynamic_cast<Object*>(&t5) );
		scene_objects.push_back( dynamic_cast<Object*>(&t6) );
		// front side
		scene_objects.push_back( dynamic_cast<Object*>(&t7) );
		scene_objects.push_back( dynamic_cast<Object*>(&t8) );
		// front side
		scene_objects.push_back( dynamic_cast<Object*>(&t9) );
		scene_objects.push_back( dynamic_cast<Object*>(&t10) );
		// bottom
		scene_objects.push_back( dynamic_cast<Object*>(&t11) );
		scene_objects.push_back( dynamic_cast<Object*>(&t12) );
		*/

		std::string s = obj_file;
		loadOBJFile(s);
		for (vec *v : verts)
		{
			
			// std::cout << v->getVecX() << ", " << v->getVecY() << ", " << v->getVecZ() << std::endl;
		}
		for (vec *v : tris)
		{
			//std::cout << verts[v->getVecX()]->getVecX() << verts[v->getVecX()]->getVecY() << verts[v->getVecX()]->getVecZ() << std::endl;
	 		//std::cout << verts[v->getVecY()]->getVecX() << verts[v->getVecY()]->getVecY() << verts[v->getVecY()]->getVecZ() << std::endl;
			//std::cout << verts[v->getVecZ()]->getVecX() <<  verts[v->getVecZ()]->getVecY() << verts[v->getVecZ()]->getVecZ() << std::endl;
			// 1, 3, 0


			/*Triangle *_t = new Triangle(
			vec (verts[v->getVecX()]->getVecX(), verts[v->getVecX()]->getVecY(), verts[v->getVecX()]->getVecZ()),
			vec (verts[v->getVecY()]->getVecX(), verts[v->getVecY()]->getVecY(), verts[v->getVecY()]->getVecZ()),
			vec (verts[v->getVecZ()]->getVecX(), verts[v->getVecZ()]->getVecY(), verts[v->getVecZ()]->getVecZ()), 
			colour);*/

			//tris_to_delete.push_back(_t);
			//scene_objects.push_back( dynamic_cast<Object*>(&*_t) );
			
			// std::cout << v->getVecX() << ", " << v->getVecY() << ", " << v->getVecZ() << std::endl;
		}

		double xamnt, yamnt;
		double tempRed, tempGreen, tempBlue;
		int aa_index;
		int thisone;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				thisone =  y * width + x;
				// start with a blank pixel
				double tempRed[aadepth*aadepth];
				double tempGreen[aadepth*aadepth];
				double tempBlue[aadepth*aadepth];

				for (int aax = 0; aax < aadepth; aax++) {
					for (int aay = 0; aay < aadepth; aay++) {
						aa_index = aay*aadepth + aax;

						srand(time(0));
						// create the ray from the camera to this pixel
						if (aadepth == 1) {

							// start with non anti-aliasing
							if (width > height) {
								// the image is wider than it is tall
								xamnt = ((x+0.5)/width)*aspectratio - (((width-height)/(double)height)/2);
								yamnt = ((height - y) + 0.5) / height;
							} else if (height > width) {
								// the image is taller than it is wide
								xamnt = (x + 0.5) / width;
								yamnt = (((height - y) + 0.5)/height)/aspectratio - (((height - width)/(double)width)/2);
							} else {
								// the image is square
								xamnt = ( x + 0.5) / width;
								yamnt = ((height - y) + 0.5)/height;
							}
						} else {
							// anti aliasing
							if (width > height) {
								// the image is wider than it is tall
								xamnt = ((x+(double)aax/((double)aadepth - 1))/width)*aspectratio - (((width-height)/(double)height)/2);
								yamnt = ((height - y) + (double)aax/((double)aadepth - 1)) / height;
							} else if (height > width) {
								// the image is taller than it is wide
								xamnt = (x + (double)aax/((double)aadepth - 1)) / width;
								yamnt = (((height - y) + (double)aax/((double)aadepth - 1))/height)/aspectratio - (((height - width)/(double)width)/2);
							} else {
								// the image is square
								xamnt = ( x + (double)aax/((double)aadepth - 1)) / width;
								yamnt = ((height - y) + (double)aax/((double)aadepth - 1))/height;
							}
						}
						vec cam_ray_origin = scene_cam.getCameraPosition();
						vec cam_ray_direction = camdir.vecAdd(camright.vecMul(xamnt - 0.5).vecAdd(camdown.vecMul(yamnt - 0.5))).normalize();
						Ray cam_ray(cam_ray_origin, cam_ray_direction);

						std::vector<double> intersections;

						for (int index = 0; index < scene_objects.size(); index++)
						{
							intersections.push_back(scene_objects.at(index)->findIntersection(cam_ray));
						}

						int index_of_winning_object = winningObjectIndex(intersections);

						if (index_of_winning_object == -1) {
							// set the background black
							tempRed[aa_index] = 0;
							tempGreen[aa_index] = 0;
							tempBlue[aa_index] = 0;
						} else {
							// index corresponds to an object in our scene
							if (intersections.at(index_of_winning_object) > accuracy) {
								// determine the position and direction vectors	
								vec intersection_position = cam_ray_origin.vecAdd(cam_ray_direction.vecMul(intersections.at(index_of_winning_object)));
								vec intersecting_ray_direction = cam_ray_direction;
								
								Colour intersection_colour = getColourAt(intersection_position, intersecting_ray_direction, scene_objects, index_of_winning_object, light_sources, accuracy, ambientlight);
								tempRed[aa_index] = intersection_colour.getColourRed();
								tempGreen[aa_index]  = intersection_colour.getColourGreen();
								tempBlue[aa_index] = intersection_colour.getColourBlue();
							}				
						}

					}
				}
				// average the pixel colour
				double totalRed = 0;
				double totalGreen = 0;
				double totalBlue = 0;

				for (int iRed = 0; iRed < aadepth*aadepth; iRed++)
				{
					totalRed = totalRed + tempRed[iRed];
				}
				for (int iGreen = 0; iGreen < aadepth*aadepth; iGreen++)
				{
					totalGreen = totalGreen + tempGreen[iGreen];
				}

				for (int iBlue = 0; iBlue < aadepth*aadepth; iBlue++)
				{
					totalBlue = totalBlue + tempBlue[iBlue];
				}
				double avgRed = totalRed/(aadepth*aadepth);
				double avgGreen = totalGreen/(aadepth*aadepth);
				double avgBlue = totalBlue/(aadepth*aadepth);

				pixels[thisone].r = avgRed;
				pixels[thisone].g = avgGreen;
				pixels[thisone].b = avgBlue;

			}
		}

		time2 = clock();
		float diff = ((float)time2 - (float)time1)/1000;
		std::cout << diff << " milliseconds" << std::endl;

		savebmp("aa_test.bmp", width, height, 72, pixels);


	//}


}
static gboolean mouse_moved(GtkWidget *widget, GdkEvent *event, gpointer user_data)
{
	
	if (event->type == GDK_MOTION_NOTIFY) {
		GdkWindow *win = gtk_widget_get_window(GTK_WIDGET(window));
		GdkScreen *screen = gdk_window_get_screen(win);
		GdkDisplay *display = gdk_window_get_display(win);
		GdkEventMotion *e = (GdkEventMotion*)event;
		if ((float)e->x >= width-1) {
			gdk_display_warp_pointer(display, screen, 0, (guint)e->y);	
			last_position = 0;
		}
		else if ((float)e->x == 0.0f) {
			gdk_display_warp_pointer(display, screen, width, (guint)e->y);	
			last_position = width;
		} else {
			float current_position = (float)e->x;
			float change_in_position = current_position - last_position;
			fYaw -= -change_in_position/width;
			last_position = current_position;
			fPitch = (float)e->y/height;
		}
	}
}

gboolean my_keypress_function (GtkWidget *widget, GdkEventKey *event, gpointer data)
{
	if (event->keyval == GDK_KEY_s) {
		is_s_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_w) {
		is_w_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_d) {
		is_d_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_a) {
		is_a_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_q) {
		is_q_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_e) {
		is_e_pressed = TRUE;
	}
	if (event->keyval == GDK_KEY_p) {
		guchar *pixels = gdk_pixbuf_get_pixels (pixbuf);
		std::ofstream out("out.ppm");
		out << "P3\n" << width << '\n' << height << '\n' << "255\n";
		for (int y = 0; y < height; y++)
		{
			for (int x = 0; x < width; x++)
			{
			    out << (int)*(pixels + y * rowstride + x * 3) << ' ';
			    out << (int)*(pixels + y * rowstride + x * 3 + 1) << ' ';
			    out << (int)*(pixels + y * rowstride + x * 3 + 2) << std::endl;
			}
		}
		render(vCamera.x, vCamera.y, vCamera.z, vLookDir.x, vLookDir.y, vLookDir.z);
		//render(lx, ly, lz, cx, cy, cz);
	}
	return GDK_EVENT_PROPAGATE;
}

static gboolean on_key_release(GtkWidget *widget, GdkEventKey *event)
{
	if (event->keyval == GDK_KEY_s) {
		is_s_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_w) {
		is_w_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_d) {
		is_d_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_a) {
		is_a_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_q) {
		is_q_pressed = FALSE;
	}
	if (event->keyval == GDK_KEY_e) {
		is_e_pressed = FALSE;
	}
	return GDK_EVENT_PROPAGATE;
}

inline void drawPixel (int x, int y, char red, char green, char blue)
{
    *(pixels + y * rowstride + x * 3) = red;
    *(pixels + y * rowstride + x * 3 + 1) = green;
    *(pixels + y * rowstride + x * 3 + 2) = blue;
}

void drawLine(int x1, int y1, int x2, int y2, int r, int g, int b)
	{
		int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
		dx = x2 - x1; dy = y2 - y1;
		dx1 = abs(dx); dy1 = abs(dy);
		px = 2 * dy1 - dx1;	py = 2 * dx1 - dy1;
		if (dy1 <= dx1)
		{
			if (dx >= 0)
				{ x = x1; y = y1; xe = x2; }
			else
				{ x = x2; y = y2; xe = x1;}

			drawPixel(x, y, r, g, b);
			
			for (i = 0; x<xe; i++)
			{
				x = x + 1;
				if (px<0)
					px = px + 2 * dy1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1; else y = y - 1;
					px = px + 2 * (dy1 - dx1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
		else
		{
			if (dy >= 0)
				{ x = x1; y = y1; ye = y2; }
			else
				{ x = x2; y = y2; ye = y1; }

			drawPixel(x, y, r, g, b);

			for (i = 0; y<ye; i++)
			{
				y = y + 1;
				if (py <= 0)
					py = py + 2 * dx1;
				else
				{
					if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1; else x = x - 1;
					py = py + 2 * (dx1 - dy1);
				}
				drawPixel(x, y, r, g, b);
			}
		}
}


void onCreate()
{
		pDepthBuffer = new float[width * height];
		s.read_png_file(file);

		matWorld = Matrix_MakeIdentity();
		meshCube.LoadFromObjectFile(obj_file, true);
		matProj = Matrix_MakeProjection(45.0f, (float)height / (float)width, 0.1f, 1000.0f);		

}

void TexturedTriangle(int x1, int y1, float u1, float v1, float w1, int x2, int y2, float u2, float v2, float w2, int x3, int y3, float u3, float v3, float w3, float shading) {

		if (y2 < y1)
		{
			std::swap(y1, y2);
			std::swap(x1, x2);
			std::swap(u1, u2);
			std::swap(v1, v2);
			std::swap(w1, w2);
		}

		if (y3 < y1)
		{
			std::swap(y1, y3);
			std::swap(x1, x3);
			std::swap(u1, u3);
			std::swap(v1, v3);
			std::swap(w1, w3);
		}

		if (y3 < y2)
		{
			std::swap(y2, y3);
			std::swap(x2, x3);
			std::swap(u2, u3);
			std::swap(v2, v3);
			std::swap(w2, w3);
		}

		int dy1 = y2 - y1;
		int dx1 = x2 - x1;
		float dv1 = v2 - v1;
		float du1 = u2 - u1;
		float dw1 = w2 - w1;

		int dy2 = y3 - y1;
		int dx2 = x3 - x1;
		float dv2 = v3 - v1;
		float du2 = u3 - u1;
		float dw2 = w3 - w1;

		float tex_u, tex_v, tex_w;

		float dax_step = 0, dbx_step = 0,
			du1_step = 0, dv1_step = 0,
			du2_step = 0, dv2_step = 0,
			dw1_step = 0, dw2_step = 0;

		if (dy1) dax_step = dx1 / (float)abs(dy1);
		if (dy2) dbx_step = dx2 / (float)abs(dy2);

		if (dy1) du1_step = du1 / (float)abs(dy1);
		if (dy1) dv1_step = dv1 / (float)abs(dy1);
		if (dy1) dw1_step = dw1 / (float)abs(dy1);

		if (dy2) du2_step = du2 / (float)abs(dy2);
		if (dy2) dv2_step = dv2 / (float)abs(dy2);
		if (dy2) dw2_step = dw2 / (float)abs(dy2);

		if (dy1)
		{
			for (int i = y1; i <= y2; i++)
			{
				int ax = x1 + (float)(i - y1) * dax_step;
				int bx = x1 + (float)(i - y1) * dbx_step;

				float tex_su = u1 + (float)(i - y1) * du1_step;
				float tex_sv = v1 + (float)(i - y1) * dv1_step;
				float tex_sw = w1 + (float)(i - y1) * dw1_step;

				float tex_eu = u1 + (float)(i - y1) * du2_step;
				float tex_ev = v1 + (float)(i - y1) * dv2_step;
				float tex_ew = w1 + (float)(i - y1) * dw2_step;

				if (ax > bx)
				{
					std::swap(ax, bx);
					std::swap(tex_su, tex_eu);
					std::swap(tex_sv, tex_ev);
					std::swap(tex_sw, tex_ew);
				}

				tex_u = tex_su;
				tex_v = tex_sv;
				tex_w = tex_sw;

				float tstep = 1.0f / ((float)(bx - ax));
				float t = 0.0f;

				for (int j = ax; j < bx; j++)
				{
					tex_u = (1.0f - t) * tex_su + t * tex_eu;
					tex_v = (1.0f - t) * tex_sv + t * tex_ev;
					tex_w = (1.0f - t) * tex_sw + t * tex_ew;
					if (tex_w > pDepthBuffer[i*width + j]) {
						int *col = s.getColour((int)((SPRITE_SIZE_W-1)*tex_u/tex_w), (int)((SPRITE_SIZE_H-1)*tex_v/tex_w));
						drawPixel(j, i, col[0]*shading, col[1]*shading, col[2]*shading);
						pDepthBuffer[i * width + j] = tex_w;
						t += tstep;
					}
				}

			}
		}

		dy1 = y3 - y2;
		dx1 = x3 - x2;
		dv1 = v3 - v2;
		du1 = u3 - u2;
		dw1 = w3 - w2;

		if (dy1) dax_step = dx1 / (float)abs(dy1);
		if (dy2) dbx_step = dx2 / (float)abs(dy2);


		du1_step = 0, dv1_step = 0, dw1_step = 0;
		if (dy1) du1_step = du1 / (float)abs(dy1);
		if (dy1) dv1_step = dv1 / (float)abs(dy1);
		if (dy1) dw1_step = dw1 / (float)abs(dy1);

		if (dy1)
		{
			for (int i = y2; i <= y3; i++)
			{
				int ax = x2 + (float)(i - y2) * dax_step;
				int bx = x1 + (float)(i - y1) * dbx_step;

				float tex_su = u2 + (float)(i - y2) * du1_step;
				float tex_sv = v2 + (float)(i - y2) * dv1_step;
				float tex_sw = w2 + (float)(i - y2) * dw1_step;

				float tex_eu = u1 + (float)(i - y1) * du2_step;
				float tex_ev = v1 + (float)(i - y1) * dv2_step;
				float tex_ew = w1 + (float)(i - y1) * dw2_step;

				if (ax > bx)
				{
					std::swap(ax, bx);
					std::swap(tex_su, tex_eu);
					std::swap(tex_sv, tex_ev);
					std::swap(tex_sw, tex_ew);
				}

				tex_u = tex_su;
				tex_v = tex_sv;
				tex_w = tex_sw;

				float tstep = 1.0f / ((float)(bx - ax));
				float t = 0.0f;

				for (int j = ax; j < bx; j++)
				{
					tex_u = (1.0f - t) * tex_su + t * tex_eu;
					tex_v = (1.0f - t) * tex_sv + t * tex_ev;
					tex_w = (1.0f - t) * tex_sw + t * tex_ew;
					if (tex_w > pDepthBuffer[i*width + j]) {
						int *col = s.getColour((int)((SPRITE_SIZE_W-1)*tex_u/tex_w), (int)((SPRITE_SIZE_H-1)*tex_v/tex_w));
						drawPixel(j, i, col[0]*shading, col[1]*shading, col[2]*shading);
						pDepthBuffer[i * width + j] = tex_w;
						t += tstep;
					}
				}
			}	
		}		
}

void drawModel(mat4x4 matWorld, mat4x4 matProj, mat4x4 matView, float x, float y)
{
std::vector<triangle> vecTrianglesToRaster;
	for (auto tri : meshCube.tris)
	{
		triangle triProjected, triTransformed, triViewed;
		matWorld = Matrix_MakeTranslation(x, y, z);
		triTransformed.p[0] = Matrix_MultiplyVector(matWorld, tri.p[0]);
		triTransformed.p[1] = Matrix_MultiplyVector(matWorld, tri.p[1]);
		triTransformed.p[2] = Matrix_MultiplyVector(matWorld, tri.p[2]);

		triTransformed.t[0] = tri.t[0];
		triTransformed.t[1] = tri.t[1];
		triTransformed.t[2] = tri.t[2];

		// Calculate triangle normal
		vec3d normal, line1, line2;

		// Get lines either side of triangle
		line1 = Vector_Sub(triTransformed.p[1], triTransformed.p[0]);
		line2 = Vector_Sub(triTransformed.p[2], triTransformed.p[0]);

		// Take cross product of lines to get normal to triangle surface
		normal = Vector_CrossProduct(line1, line2);

		// You normally need to normalise a normal!
		normal = Vector_Normalise(normal);

		vec3d vCameraRay = Vector_Sub(triTransformed.p[0], vCamera);

		if (Vector_DotProduct(normal, vCameraRay) < 0.0f) {

			// Illumination

			light_direction = Vector_Normalise(light_direction);

			// how "aligned are light direction and triangle surface normal?
			float dp = std::max(0.1f, Vector_DotProduct(light_direction, normal));

			// Convert world space -> view space
			triViewed.p[0] = Matrix_MultiplyVector(matView, triTransformed.p[0]);
			triViewed.p[1] = Matrix_MultiplyVector(matView, triTransformed.p[1]);	
			triViewed.p[2] = Matrix_MultiplyVector(matView, triTransformed.p[2]);			
			triViewed.col = triTransformed.col;
			triViewed.t[0] = triTransformed.t[0];
			triViewed.t[1] = triTransformed.t[1];
			triViewed.t[2] = triTransformed.t[2];
			int nClippedTriangles = 0;
			triangle clipped[2];
			nClippedTriangles = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.1f }, { 0.0f, 0.0f, 1.0f }, triViewed, clipped[0], clipped[1]); 

			for (int n = 0; n < nClippedTriangles; n++)
			{

				// project triangles from 3D -> 2D
				triProjected.p[0] = Matrix_MultiplyVector(matProj, clipped[n].p[0]);
				triProjected.p[1] = Matrix_MultiplyVector(matProj, clipped[n].p[1]);
				triProjected.p[2] = Matrix_MultiplyVector(matProj, clipped[n].p[2]);
				triProjected.col = clipped[n].col;
				triProjected.t[0] = clipped[n].t[0];
				triProjected.t[1] = clipped[n].t[1];
				triProjected.t[2] = clipped[n].t[2];

				triProjected.t[0].u = triProjected.t[0].u / triProjected.p[0].w;
				triProjected.t[1].u = triProjected.t[1].u / triProjected.p[1].w;
				triProjected.t[2].u = triProjected.t[2].u / triProjected.p[2].w;
				
				triProjected.t[0].v = triProjected.t[0].v / triProjected.p[0].w;
				triProjected.t[1].v = triProjected.t[1].v / triProjected.p[1].w;
				triProjected.t[2].v = triProjected.t[2].v / triProjected.p[2].w;

				triProjected.t[0].w = 1.0f / triProjected.p[0].w;
				triProjected.t[1].w = 1.0f / triProjected.p[1].w;
				triProjected.t[2].w = 1.0f / triProjected.p[2].w;
				// scale into view

				triProjected.p[0] = Vector_Div(triProjected.p[0], triProjected.p[0].w);
				triProjected.p[1] = Vector_Div(triProjected.p[1], triProjected.p[1].w);
				triProjected.p[2] = Vector_Div(triProjected.p[2], triProjected.p[2].w);

				triProjected.p[0].x *= -1.0f;
				triProjected.p[1].x *= -1.0f;
				triProjected.p[2].x *= -1.0f;
				triProjected.p[0].y *= -1.0f;
				triProjected.p[1].y *= -1.0f;
				triProjected.p[2].y *= -1.0f;		

				vec3d vOffsetView = { 1, 1, 0 };

				triProjected.p[0] = Vector_Add(triProjected.p[0], vOffsetView);
				triProjected.p[1] = Vector_Add(triProjected.p[1], vOffsetView);
				triProjected.p[2] = Vector_Add(triProjected.p[2], vOffsetView);
				triProjected.p[0].x *= 0.5f * (float)width;
				triProjected.p[0].y *= 0.5f * (float)height;
				triProjected.p[1].x *= 0.5f * (float)width;
				triProjected.p[1].y *= 0.5f * (float)height;
				triProjected.p[2].x *= 0.5f * (float)width;
				triProjected.p[2].y *= 0.5f * (float)height;
				triProjected.col = dp;
				
				vecTrianglesToRaster.push_back(triProjected);
			}
		}
	}
	/*std::sort(vecTrianglesToRaster.begin(), vecTrianglesToRaster.end(), [](triangle &t1, triangle &t2)
	{
		float z1 = (t1.p[0].z + t1.p[1].z + t1.p[2].z) / 3.0f;
		float z2 = (t2.p[0].z + t2.p[1].z + t2.p[2].z) / 3.0f;

		return z1 > z2;
	});*/

	for (auto &triToRaster : vecTrianglesToRaster)
	{

		triangle clipped[2];
		std::list<triangle> listTriangles;
		listTriangles.push_back(triToRaster);
		int nNewTriangles = 1;

		for (int p = 0; p < 4; p++)
		{
			int nTrisToAdd = 0;
			while (nNewTriangles > 0)
			{
				triangle test = listTriangles.front();
				listTriangles.pop_front();
				nNewTriangles--;

				switch (p)
				{
					case 0:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 1:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, (float)height - 1, 0.0f }, { 0.0f, -1.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 2:	nTrisToAdd = Triangle_ClipAgainstPlane({ 0.0f, 0.0f, 0.0f }, { 1.0f, 0.0f, 0.0f }, test, clipped[0], clipped[1]); break;
					case 3: nTrisToAdd = Triangle_ClipAgainstPlane({ (float)width - 1, 0.0f, 0.0f }, { -1.0f, 0.0f, 0.0f }, test, clipped[0], clipped[1]); break;
				}
				for (int w = 0; w < nTrisToAdd; w++)
					listTriangles.push_back(clipped[w]);
			}
			nNewTriangles = listTriangles.size();
		}

		for (auto &t : listTriangles)
		{
			allTriangles.push_back(t);
		}
	}
}



gboolean game_loop (GtkWidget *widget, GdkFrameClock *clock, gpointer data)
{
	if (is_w_pressed) {
		vec3d vForward_ = {vForward.x, 0, vForward.z};
		vCamera = Vector_Add(vCamera, vForward_);
	}
	if (is_a_pressed) {
		vCamera = Vector_Sub(vCamera, vLeft);
	}
	if (is_s_pressed) {
		vec3d vForward_ = {vForward.x, 0, vForward.z};
		vCamera = Vector_Sub(vCamera, vForward_);
	}
	if (is_d_pressed) {
		vCamera = Vector_Sub(vCamera, vRight);
	}
	if (is_q_pressed) {
		vCamera.y -= 0.1f;
	}
	if (is_e_pressed) {
		vCamera.y += 0.1f;
	}
	gdk_pixbuf_fill(pixbuf, 0);
	for (int i = 0; i < width * height; i++) {
		pDepthBuffer[i] = 0.0f;
	}

	matRotZ = Matrix_MakeRotationZ(fTheta * 0.5f);
	matRotX = Matrix_MakeRotationX(fTheta);
	matTrans = Matrix_MakeTranslation(0.0f, 0.0f, 16.0f);
	matWorld = Matrix_MultiplyMatrix(matWorld, matTrans);
	vUp = { 0,1,0 };
	vTarget = { 0,0,1 };
	matCameraRotY = Matrix_MakeRotationY(fYaw);
	matCameraRotX = Matrix_MakeRotationX(fPitch);
	matCameraRot = Matrix_MultiplyMatrix(matCameraRotX, matCameraRotY);
	vLookDir = Matrix_MultiplyVector(matCameraRot, vTarget);
	// std::cout << vLookDir.x << std::endl;
	vForward = Vector_Mul(vLookDir, 0.1f);
	vTarget = Vector_Add(vCamera, vLookDir);
	vLeft = Vector_CrossProduct(vLookDir, vUp);
	vLeft = Vector_Mul(vLeft, 0.1f);
	vRight = Vector_CrossProduct(vLookDir, vUp);
	vRight = Vector_Mul(vRight, -0.1f);
	matCamera = Matrix_PointAt(vCamera, vTarget, vUp);
	matView = Matrix_QuickInverse(matCamera);
	drawModel(matWorld, matProj, matView, 0,0);
	drawModel(matWorld, matProj, matView, 2,0);
	drawModel(matWorld, matProj, matView, 2,2.001);
	drawModel(matWorld, matProj, matView, 0,2.001);
	/*std::sort(allTriangles.begin(), allTriangles.end(), [](triangle &t1, triangle &t2)
	{
		float z1 = (t1.p[0].z + t1.p[1].z + t1.p[2].z) / 3.0f;
		float z2 = (t2.p[0].z + t2.p[1].z + t2.p[2].z) / 3.0f;

		return z1 > z2;
	});*/
	for (auto &t : allTriangles)
	{
		TexturedTriangle(t.p[0].x, t.p[0].y, t.t[0].u, t.t[0].v, t.t[0].w,
						t.p[1].x, t.p[1].y, t.t[1].u, t.t[1].v, t.t[1].w,
						t.p[2].x, t.p[2].y, t.t[2].u, t.t[2].v, t.t[2].w, t.col);
	}
	allTriangles.clear();

    drawPixel(width/2, height/2, 255, 255, 255);
    drawPixel(width/2-1, height/2, 255, 255, 255);
    drawPixel(width/2+1, height/2, 255, 255, 255);
    drawPixel(width/2, height/2-1, 255, 255, 255);
    drawPixel(width/2, height/2+1, 255, 255, 255);
    gtk_image_set_from_pixbuf (image, pixbuf);  
    return 1;
} 

int main(int argc, char *argv[])
{
	if (argc != 5) {
		std::cout << "Error: ./model_viewer [/path/to/texture.png] [TEXTURE_WIDTH] [TEXTURE_HEIGHT] [/path/to/model.obj]" << std::endl;
		system("exit");
	}
    gtk_init (NULL, NULL);
	file = (const char*)argv[1];
	SPRITE_SIZE_W = std::atoi(argv[2]);
	SPRITE_SIZE_H = std::atoi(argv[3]);
	obj_file = (const char*)argv[4];
	onCreate();

    GtkWidget *container_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
	vbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    menuBar = gtk_menu_bar_new();
    fileMenu = gtk_menu_new();
    fileMi = gtk_menu_item_new_with_label("File");
    quitMi = gtk_menu_item_new_with_label("Quit");
    gtk_menu_item_set_submenu(GTK_MENU_ITEM(fileMi), fileMenu);
    gtk_menu_shell_append(GTK_MENU_SHELL(fileMenu), quitMi);
    gtk_menu_shell_append(GTK_MENU_SHELL(menuBar), fileMi);
    gtk_box_pack_start(GTK_BOX(vbox), menuBar, TRUE, TRUE, 0);
    
    g_signal_connect(G_OBJECT(quitMi), "activate", G_CALLBACK(gtk_main_quit), NULL);


    image = GTK_IMAGE (gtk_image_new());
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_widget_add_events(window, GDK_KEY_PRESS_MASK);
	gtk_widget_add_events(window, GDK_BUTTON_PRESS_MASK);
	gtk_widget_add_events(GTK_WIDGET(container_box), GDK_SCROLL_MASK);
    box = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start (GTK_BOX (box), GTK_WIDGET(image), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(container_box), GTK_WIDGET(vbox), TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(container_box), GTK_WIDGET(box), TRUE, TRUE, 0);

    gtk_container_add (GTK_CONTAINER (window), container_box);
    g_signal_connect(G_OBJECT(container_box), "scroll-event", G_CALLBACK(mouse_scroll), NULL);
    g_signal_connect(G_OBJECT(window), "motion-notify-event", G_CALLBACK(mouse_moved), NULL);
	g_signal_connect (window, "key_press_event", G_CALLBACK (my_keypress_function), NULL);
	g_signal_connect (window, "key_release_event", G_CALLBACK (on_key_release), NULL);
	g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    gtk_widget_add_tick_callback(window, game_loop, NULL, NULL);
	//gtk_window_fullscreen(GTK_WINDOW(window));
	GdkWindow *win = gtk_widget_get_window(GTK_WIDGET(window));
	gtk_widget_set_events(window, GDK_POINTER_MOTION_MASK);
	gtk_widget_show_all (window);
    gtk_main();
    delete[] pDepthBuffer;
	for (vec *v : verts)
	{
		// std::cout << v->getVecX() << ", " << v->getVecY() << ", " << v->getVecZ() << std::endl;
		delete v;
	}
	for (vec *v : tris)
	{
		// std::cout << v->getVecX() << ", " << v->getVecY() << ", " << v->getVecZ() << std::endl;
		delete v;
	}
	for (Triangle *t: tris_to_delete)
	{
		delete t;
	}
}
