import pygame
import math
from math import cos, sin

pygame.init()
w=400;h=400;cx=w//2;cy=h//2
screen = pygame.display.set_mode((w,h))
clock = pygame.time.Clock()

running = True
white = (255,255,255)
black = (0,0,0)
verts = (-1,-1,1),(1,-1,1),(1,1,1),(-1,1,1),(-1,-1,-1),(1,-1,-1),(1,1,-1),(-1,1,-1)
edges = (0,1),(1,2),(2,3),(3,0),(4,5),(5,6),(6,7),(7,4),(0,4),(1,5),(2,6),(3,7)

angle = 0
depth = 200
f1 = cy
f2 = 50
scale = 100
dx = 0
dy = 0

while running:

	if (angle == 360):
		angle = 1

	rad = math.pi / 180 * angle

	for event in pygame.event.get():
		if event.type == pygame.QUIT:
			running = False



	if  pygame.key.get_pressed()[pygame.K_d]:
		angle -= 1
		#dx += 10

	if  pygame.key.get_pressed()[pygame.K_a]:
		angle += 1
		#dx -= 10


	screen.fill(black)

	rotated_verts = []

	for vert in verts:
		# scale to screen
		x = depth*vert[0]+dx
		y = depth*vert[1]+dy
		z = vert[2] + 5
		# projection
		x /= z
		y /= z
		rotated_verts.append((int(x)+cx,int(y)+cy))
		pygame.draw.circle(screen, white, (int(x)+cx,int(y)+cy), 3)
	print (angle)

	#for vert in verts:
	#	x = vert[0] * depth / ( vert[2] + depth)
	#	y = vert[1] * depth / ( vert[2] + depth)
	#	pygame.draw.circle(screen, white, (x,y), 3)

	#

	rotated_verts.append((cx + depth * (sin(rad)), cy + depth * cos(rad)))
	rotated_verts.append((cx + depth * (cos(rad)), cy - depth * sin(rad)))
	rotated_verts.append((cx + depth * (cos(rad)), depth + depth * sin(rad)))
	rotated_verts.append((cx + depth * (sin(rad)), depth - depth * cos(rad)))

	rotated_verts.append((cx - depth * (cos(rad)), cy + depth * sin(rad)))
	rotated_verts.append((cx - depth * (sin(rad)), cy - depth * cos(rad)))
	rotated_verts.append((cx - depth * (sin(rad)), depth + depth * cos(rad)))
	rotated_verts.append((cx - depth * (cos(rad)), depth - depth * sin(rad)))

	for edge in edges:
		pygame.draw.line(screen, white, rotated_verts[edge[0]], rotated_verts[edge[1]])




	pygame.display.flip()
	clock.tick(60)
