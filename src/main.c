// gcc test.c `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` -o test
#include <gtk/gtk.h>
#include <stdlib.h>
#include <math.h>

int v_sz = 1;
int f_sz = 1;
double *verts;
unsigned int *faces;
unsigned char col[3] = {0,0,255};
const float thirty_fps = 1000/30;

struct mat4x4
{
	float m[4][4];
};


void objectImporter(const char* filename)
{
	FILE *fp = fopen(filename, "r");
	char status;
	char s[255];

	verts = malloc(v_sz * sizeof(double) * 3);
	faces = malloc(f_sz * sizeof(unsigned int) * 3);
	while (1)
	{
		status = fgets(s, sizeof(s), fp);
		if (!status)
			break;
		if (s[0] == 'f')
		{
			faces = (unsigned int *)realloc(faces, ++f_sz * sizeof(unsigned int) * 3);
			int offset = 3;
			for (char *token = strtok(&s[1], " "); token != NULL; token = strtok(NULL, " "))
			{
				faces[f_sz * 3 - offset] = atoi(token);
				offset--;
				//printf("%d ",atoi(token));
			}
			//printf("\n");
		}

		if (s[0] == 'v')
		{
			verts = (double *)realloc(verts, ++v_sz * sizeof(double) * 3);
			int offset = 3;
			for (char *token = strtok(&s[1], " "); token != NULL; token = strtok(NULL, " "))
			{
				verts[v_sz * 3 - offset] = atof(token);
				//printf("%f ", verts[v_sz * 3 - offset]);
				offset--;

			}
			//printf("\n");
		}
	}
	fclose(fp);
}

static void
put_pixel (GdkPixbuf *pixbuf, int x, int y, guchar red, guchar green, guchar blue, guchar alpha)
{
    int width, height, rowstride, n_channels;
    guchar *pixels, *p;

    n_channels = gdk_pixbuf_get_n_channels (pixbuf);
    g_assert (gdk_pixbuf_get_colorspace (pixbuf) == GDK_COLORSPACE_RGB);
    g_assert (gdk_pixbuf_get_bits_per_sample (pixbuf) == 8);
    g_assert (n_channels == 3);

    width = gdk_pixbuf_get_width (pixbuf);

    height = gdk_pixbuf_get_height (pixbuf);
	//printf("%d ", width);

    g_assert (x >= 0 && x <= width);
    g_assert (y >= 0 && y <= height);

    rowstride = gdk_pixbuf_get_rowstride (pixbuf);
    pixels = gdk_pixbuf_get_pixels (pixbuf);

    p = pixels + y * rowstride + x * n_channels;
    p[0] = red;
    p[1] = green;
    p[2] = blue;
}


void drawLine(GdkPixbuf *pixbuf, int x1, int y1, int x2, int y2, unsigned char* col)
{	
	int x, y, dx, dy, dx1, dy1, px, py, xe, ye, i;
	dx = x2 - x1; dy = y2 - y1;
	dx1 = abs(dx); dy1 = abs(dy);
	px = 2 * dy1 - dx1;	py = 2 * dx1 - dy1;
	if (dy1 <= dx1)
	{
		if (dx >= 0)
			{ x = x1; y = y1; xe = x2; }
		else
			{ x = x2; y = y2; xe = x1;}

		put_pixel (pixbuf, x, y, col[0], col[1], col[2], 255);
		
		for (i = 0; x<xe; i++)
		{
			x = x + 1;
			if (px<0)
				px = px + 2 * dy1;
			else
			{
				if ((dx<0 && dy<0) || (dx>0 && dy>0)) y = y + 1; else y = y - 1;
				px = px + 2 * (dy1 - dx1);
			}
			put_pixel (pixbuf, x, y, col[0], col[1], col[2], 255);
		}
	}
	else
	{
		if (dy >= 0)
			{ x = x1; y = y1; ye = y2; }
		else
			{ x = x2; y = y2; ye = y1; }

		put_pixel (pixbuf, x, y, col[0], col[1], col[2], 255);

		for (i = 0; y<ye; i++)
		{
			y = y + 1;
			if (py <= 0)
				py = py + 2 * dx1;
			else
			{
				if ((dx<0 && dy<0) || (dx>0 && dy>0)) x = x + 1; else x = x - 1;
				py = py + 2 * (dx1 - dy1);
			}
			put_pixel (pixbuf, x, y, col[0], col[1], col[2], 255);
		}
	}
}

float* mulMatrix(float *v, struct mat4x4 m)
{
	float vx = v[0];
	float vy = v[1];
	float vz = v[2];

	float _vx;
	float _vy;
	float _vz;
	_vx = vx * m.m[0][0] + vy * m.m[1][0] + vz * m.m[2][0] + m.m[3][0];
	_vy = vx * m.m[0][1] + vy * m.m[1][1] + vz * m.m[2][1] + m.m[3][1];
	_vz = vx * m.m[0][2] + vy * m.m[1][2] + vz * m.m[2][2] + m.m[3][2];
	float w = vx * m.m[0][3] + vy * m.m[1][3] + vz * m.m[2][3] + m.m[3][3];
	if (w != 0.0f)
	{
		_vx /= w; _vy /= w; _vz /= w; 
	}
	static float _v[3];

	_v[0] = _vx;
	_v[1] = _vy;
	_v[2] = _vz;
 
	return _v; 

}

float* getPoint(int i)
{
	float x = verts[i * 3 + 0];
	float y = verts[i * 3 + 1];
	float z = verts[i * 3 + 2];
	static float v[3];
	v[0] = x;
	v[1] = y;
	v[2] = z;
	
	return v;
}

struct mat4x4 m;
struct mat4x4 m2;
struct mat4x4 m3;
float fElapsedTime = 0;
static gboolean game_loop (gpointer user_data) {
	fElapsedTime += thirty_fps;
    GdkPixbuf *pixbuf;
    pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, 320, 240);
    for (int i = 0; i <320; i++)
    {
        for (int j = 0; j < 240; j++)
        {
            put_pixel (pixbuf, i, j, 0, 0, 0, 255);
        }
    }

	for (int i = 1; i < f_sz; i++)
	{

    	// for some reason, v1, v2, v3 pointers change between the print statement
		// and the drawline call
		// so storing them on the stack is necessary to keep correct values.
		float *v1 = getPoint(faces[i * 3 + 0]);
		float v1x = v1[0];
		float v1y = v1[1];
		float v1z = v1[2] + 3.0f;

		//float *_v1 = mulMatrix(v1, m);
		float _v1x;// = _v1[0];
		float _v1y;// = _v1[1];
		float _v1z;// = _v1[2];
		_v1x = v1x * m.m[0][0] + v1y * m.m[1][0] + v1z * m.m[2][0] + m.m[3][0];
		_v1y = v1x * m.m[0][1] + v1y * m.m[1][1] + v1z * m.m[2][1] + m.m[3][1];
		_v1z = v1x * m.m[0][2] + v1y * m.m[1][2] + v1z * m.m[2][2] + m.m[3][2];
		float w1 = v1x * m.m[0][3] + v1y * m.m[1][3] + v1z * m.m[2][3] + m.m[3][3];
		if (w1 != 0.0f)
		{
			_v1x /= w1; _v1y /= w1; _v1z /= w1; 
		}
		float *v2 = getPoint(faces[i * 3 + 1]);
		float v2x = v2[0];
		float v2y = v2[1];
		float v2z = v2[2] + 3.0f;
		float _v2x;
		float _v2y;	
		float _v2z;
		_v2x = v2x * m.m[0][0] + v2y * m.m[1][0] + v2z * m.m[2][0] + m.m[3][0];
		_v2y = v2x * m.m[0][1] + v2y * m.m[1][1] + v2z * m.m[2][1] + m.m[3][1];
		_v2z = v2x * m.m[0][2] + v2y * m.m[1][2] + v2z * m.m[2][2] + m.m[3][2];
		float w2 = v2x * m.m[0][3] + v2y * m.m[1][3] + v2z * m.m[2][3] + m.m[3][3];
		if (w2 != 0.0f)
		{
			_v2x /= w2; _v2y /= w2; _v2z /= w2; 
		}
		float *v3 = getPoint(faces[i * 3 + 2]);
		float v3x = v3[0];
		float v3y = v3[1];
		float v3z = v3[2] + 3.0f;
		float _v3x;
		float _v3y;	
		float _v3z;
		_v3x = v3x * m.m[0][0] + v3y * m.m[1][0] + v3z * m.m[2][0] + m.m[3][0];
		_v3y = v3x * m.m[0][1] + v3y * m.m[1][1] + v3z * m.m[2][1] + m.m[3][1];
		_v3z = v3x * m.m[0][2] + v3y * m.m[1][2] + v3z * m.m[2][2] + m.m[3][2];
		float w3 = v3x * m.m[0][3] + v3y * m.m[1][3] + v3z * m.m[2][3] + m.m[3][3];
		if (w3 != 0.0f)
		{
			_v3x /= w3; _v3y /= w3; _v3z /= w3; 
		}

		_v1x += 1.0f; _v1y += 1.0f; _v1z += 1.0f;
		_v2x += 1.0f; _v2y += 1.0f; _v2z += 1.0f;
		_v3x += 1.0f; _v3y += 1.0f; _v3z += 1.0f;

		_v1x *= 0.5 * 320.0; _v1y *= 0.5 * 240.0;
		_v2x *= 0.5 * 320.0; _v2y *= 0.5 * 240.0;
		_v3x *= 0.5 * 320.0; _v3y *= 0.5 * 240.0;
		_v1x = (unsigned int)_v1x;
		_v1y = (unsigned int)_v1y;
		_v1z = (unsigned int)_v1z;

		_v2x = (unsigned int)_v2x;
		_v2y = (unsigned int)_v2y;
		_v2z = (unsigned int)_v2z;

		_v3x = (unsigned int)_v3x;
		_v3y = (unsigned int)_v3y;
		_v3z = (unsigned int)_v3z;

		printf("%f, %f, %f \n", _v1x, _v1y, _v1z);
		printf("%f, %f, %f \n", _v2x, _v2y, _v2z);
		printf("%f, %f, %f \n", _v3x, _v3y, _v3z);

		m2.m[0][0] = cosf(fElapsedTime);
		m2.m[0][1] = sinf(fElapsedTime);
		m2.m[1][0] = -sinf(fElapsedTime);
		m2.m[1][1] = cosf(fElapsedTime);
		m2.m[2][2] = 1;
		m2.m[3][3] = 1;

		m3.m[0][0] = 1;
		m3.m[0][1] = cosf(fElapsedTime * 0.5f);
		m3.m[1][0] = sinf(fElapsedTime * 0.5f);
		m3.m[1][1] = -sinf(fElapsedTime * 0.5f);
		m3.m[2][2] = cosf(fElapsedTime * 0.5f);
		m3.m[3][3] = 1;

		drawLine(pixbuf, _v1x, _v1y, _v2x, _v2y, col);
		drawLine(pixbuf, _v2x, _v2y, _v3x, _v3y, col);
		drawLine(pixbuf, _v3x, _v3y, _v1x, _v1y, col);
	}

    //unsigned char col[3] = {0,0,255};
    //drawLine(pixbuf, 0,0, 100, 100, col);

    gtk_image_set_from_pixbuf (GTK_IMAGE (user_data), pixbuf);  
    return G_SOURCE_CONTINUE;
}


int
main (int argc, char *argv[])
{
    GtkWidget *window;
    gtk_init (&argc, &argv);
    GtkWidget *box;
    GtkWidget *image;
    objectImporter("/home/ubuntu/Documents/cube.obj");

	for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
			m.m[i][j] = 0.0f;
	float fNear = 0.1f;
	float fFar = 1000.0f;
	float fFov = 90.0f;
	float fAspectRatio = 240.0 / 320.0;
	float fFovRad = 1.0f / tanf(fFov * 0.5f / 180.0f * 3.14159f);
    m.m[0][0] = fAspectRatio * fFovRad;
	m.m[1][1] = fFovRad;
	m.m[2][2] = fFar / (fFar - fNear);
	m.m[3][2] = (-fFar * fNear) / (fFar - fNear);
	m.m[2][3] = 1.0f;
	m.m[3][3] = 0.0f;

	image = gtk_image_new();

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "game_loop_test.c");

    box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start (GTK_BOX (box), image, TRUE, TRUE, 0);
    gtk_container_add (GTK_CONTAINER (window), box);

    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);

    g_timeout_add (thirty_fps, game_loop, image);
    gtk_widget_show_all (window);

    gtk_main();

	free(verts);
	free(faces);
    return 0;
}
