#include <stdio.h>
#include <boost/python.hpp>

using namespace boost::python;

typedef struct sCommClass {
	int (*open)(struct sCommClass *self, char *fspec);
} tCommClass;

static int tcpOpen (tCommClass *tcp, char *fspec) {
	printf("Opening TCP: %s\n", fspec);
	return 0;
}

static int tcpInit (tCommClass *tcp) {
	tcp->open = &tcpOpen;
	return 0;
}

BOOST_PYTHON_MODULE(OOPc)
{
	class_<tCommClass>("tCommClass")
	.def("tcpInit", args("self"));
}

int main()
{
	int status;
	tCommClass commTcp, commHttp;

	tcpInit (&commTcp);
	status = (commTcp.open)(&commTcp, "bigiron.box.com:5000");

	return 0;
}
