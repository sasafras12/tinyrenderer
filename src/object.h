#ifndef _OBJECT_H
#define _OBJECT_H

#include "ray.h"
#include "vec.h"
#include "colour.h"

class Object {
public:
	Object();

	// method functions
	virtual Colour getColour() { return Colour(0.0, 0.0, 0.0, 0); }

	virtual vec getNormalAt(vec intersection_position) {
		return vec(0,0,0);
	}

	virtual double findIntersection(Ray ray) {
		
		return 0;
	}
};

Object::Object() {}

#endif
