#ifndef _COLOUR_H
#define _COLOUR_H

#include <math.h>

class Colour {
	double red;
	double green;
	double blue;
	double special;

public:
	Colour();
	Colour(double, double, double, double);

	// method functions
	double getColourRed() { return red; }
	double getColourGreen() { return  green; }
	double getColourBlue() { return blue; }
	double getColourSpecial() { return special; }

	double setColourRed(double redValue) { red = redValue; }
	double setColourGreen(double greenValue) { green = greenValue; }
	double setColourBlue(double blueValue) { blue = blueValue; }
	double setColourSpecial(double specialValue) { special = specialValue; }
	double brightness() {
		return (red + green + blue)/3;
	}
	Colour colourScalar(double scalar) {
		return Colour(red*scalar, green*scalar, blue*scalar, special);
	}

	Colour colourAdd(Colour colour) {
		return Colour (red + colour.getColourRed(), green + colour.getColourGreen(), blue + colour.getColourBlue(), special);
	}

	Colour colourMultiply(Colour colour) {
		return Colour(red * colour.getColourRed(), green * colour.getColourGreen(), blue * colour.getColourBlue(), special);
	}

	Colour colourAverage(Colour colour) {
		return Colour((red + colour.getColourRed())/2, (green + colour.getColourGreen())/2, (blue + colour.getColourBlue())/2, special);
	}

	Colour clip() {
		double alllight = red + green + blue;
		double excesslight = alllight - 3;
		if (excesslight > 0) {
			red = red + excesslight*(red/alllight);
			green = green + excesslight*(green/alllight);
			blue = blue + excesslight*(blue/alllight);
		}
		if (red > 1) {
			red = 1;
		}
		if (green > 1) {
			green= 1;
		}
		if (blue > 1) {
			blue = 1;
		}
		if (red < 0) {
			red = 0;
		}
		if (green < 0) {
			green = 0;
		}
		if (blue < 0) {
			blue = 0;
		}
		return Colour(red, green, blue, special);
	}
};
Colour::Colour () {
	red = 0.5;
	green = 0.5;
	blue = 0.5;
}

Colour::Colour (double r, double g, double b, double s) {
	red = r;
	green = g;
	blue = b;
	special = s;
}

#endif
