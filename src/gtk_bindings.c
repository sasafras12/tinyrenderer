#include <boost/python.hpp>
#include <gtk/gtk.h>

using namespace boost::python;
// gcc gtk_bindings.c -g `pkg-config --cflags gtk+-3.0` `pkg-config --libs gtk+-3.0` -o gtk_bindings

GdkPixbuf *pixbuf = gdk_pixbuf_new (GDK_COLORSPACE_RGB, FALSE, 8, 640, 480);
GtkWidget *image;
GtkImage *_image;

static int rowstride = gdk_pixbuf_get_rowstride (pixbuf);;
static guchar *pixels = gdk_pixbuf_get_pixels (pixbuf), *p;
int width, height;
int n_channels;



void drawPixel (const unsigned int x, const unsigned int y, const unsigned char red, const unsigned char green, const unsigned char blue)
{
    p = pixels + y * rowstride + x * 3;
    p[0] = red;
    p[1] = green;
    p[2] = blue;
}

gboolean game_loop (GtkWidget *widget, GdkFrameClock *frame_clock, gpointer user_data) {
    for (int i = 0; i < 640; i++)
	{
		for (int j = 0; j < 480; j++)
		{
			drawPixel(i, j, rand() % 255, rand() % 255, rand() % 255);
		}
	}
    //PyObject *func = (PyObject *)user_data;
    //boost::python::call<void>(func);
    gtk_image_set_from_pixbuf (_image, pixbuf);  
    return G_SOURCE_CONTINUE;
} 

void run(PyObject *func)
{
    GtkWidget *window;
    gtk_init (NULL, NULL);
    GtkWidget *box;

    image = gtk_image_new();
    _image = GTK_IMAGE (image);

    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    gtk_window_set_title (GTK_WINDOW (window), "Welcome to GTKgame!");

    box = gtk_box_new (GTK_ORIENTATION_VERTICAL, 5);
    gtk_box_pack_start (GTK_BOX (box), image, TRUE, TRUE, 0);
    gtk_container_add (GTK_CONTAINER (window), box);
    g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
	gtk_widget_add_tick_callback(window, game_loop, func, NULL);
    //g_timeout_add (1000/20, game_loop, func);
    gtk_widget_show_all (window);

    gtk_main();
}

BOOST_PYTHON_MODULE(GtkGraphics)
{
        def("run", run, args("func"));
        def("drawPixel", drawPixel, args("x", "y", "red", "green", "blue"));
        
}
