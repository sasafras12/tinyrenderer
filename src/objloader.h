#ifndef _OBJLOADER_H
#define _OBJLOADER_H

#include <strstream>
#include "ray.h"
#include "vec.h"
#include "colour.h"

std::vector<vec*> verts;
std::vector<vec*> tris;

bool loadOBJFile(std::string sFilename)
{
	std::ifstream f((std::string)sFilename);
		if (!f.is_open()) {
			return false;
		}

	while (!f.eof())
	{
		char line[128];
		f.getline(line, 128);
		std::strstream s;
		s << line;

		char junk;

		if (line[0] == 'v' && line[1] != 't')
		{
			float x, y, z;
			s >> junk >> x >> y >> z;
			vec *v = new vec(x, y, z);
			verts.push_back(v);
		}
		if (line[0] == 'f')
		{
			s >> junk;
			s >> junk;
			char *token;
			const char* delim = " ";
			const char* delim2 = "/";
			std::vector<char*> tokens;
			token = strtok(s.str(), delim);
			while (token != NULL) {
				tokens.push_back(token);
				token = strtok(NULL, delim);
			}

			token = strtok(tokens[1], delim2);
			int x = std::atoi(token);
			token = strtok(tokens[2], delim2);
			int y = std::atoi(token);
			token = strtok(tokens[3], delim2);
			int z = std::atoi(token);
			// switching normals / winding direction hack below (x, y, z) becomes (y, x, z)
			vec *v = new vec(y - 1, x - 1, z - 1);
			tris.push_back(v);
		}

	}
} 

#endif
